/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <vector>
#include "test_runner.h"
#include <assert.h> 
//#include "log_data.h"

using namespace std;


bool test_test() {
	return 1 == 1;
}


bool Tests::get_test(string test_name) {
	int test_id = test_names.size();
	for (int i = 0; i < test_names.size(); i++) {
		if (test_names[i] == test_name) {
			test_id = i;
			break;
		}
	}
	switch (test_id) {
	case 0: return test_test();
	//case 1: return LogData::xt_experiment();
	default: return "test_not defined";
	}
}

bool Tests::run_test(string test_name) {
	try {
		assert(get_test(test_name));
		cout << test_name << " passed" << endl;
		return true;
	}
	catch (const std::exception& e) {
		cout << test_name << " failed: " << e.what() << endl;
		return false;
	}
}

bool Tests::run_all() {
	bool all_passed = true;
	for (int i = 0; i < test_names.size(); i++) {
		if (!run_test(test_names[i])) all_passed = false;
		
	}
	return all_passed;
}