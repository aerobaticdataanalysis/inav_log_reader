to install:
activate virtual env, navigate to fast_binary_tools folder, 'pip install .'

requirements:
pybind11,
setuptools,
visual studio 2015 build tools


to run:
from fast_binary_tools import Parser
reader = Parser()
reader.load_log('path to log.TXT')


names = reader.get_field_names(int field_id)
names = list of field names for the given frame type

value = reader.next_frame()
gives:
value.frame_type = int: id of frame type
value.values = list of values for the frame type specified



value = reader.next_dict()
gives:
value.time = long: time of frame
value.pairs = dict: of field name, value pairs

