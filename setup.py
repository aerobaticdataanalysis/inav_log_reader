'''
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
'''

from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext
import sys
import setuptools

#cpp_args = ['-std=c++11', '-stdlib=libc++', '-mmacosx-version-min=10.7']

sfc_module = Extension(
    'inav_log_reader', sources = ['module.cpp', 'frames.cpp', 'parser.cpp', 'stream.cpp', 'tools.cpp', 'test_runner.cpp', 'log.cpp'], 
    include_dirs=['pybind11/include'],
    language='c++',
    extra_link_args=['-L/usr/lib/x86_64-linux-gnu/']
    )

setup(
    name = 'inav_log_reader',
    version = '1.0',
    description = 'Python package to read inav binary logs C++ extension (PyBind11)',
    ext_modules = [sfc_module],
)