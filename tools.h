/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <string>
#include <vector>

int32_t zigzagDecode(uint32_t value);

int32_t signExtend24Bit(uint32_t u);

int32_t signExtend14Bit(uint16_t word);

int32_t signExtend6Bit(uint8_t byte);

int32_t signExtend4Bit(uint8_t nibble);

int32_t signExtend2Bit(uint8_t byte);

bool startsWith(std::string s, std::string checkStartsWith);

bool endsWith(std::string s, std::string checkEndsWith);

std::vector<std::string> split(const std::string& str, const std::string& delim);

std::vector<int> split_int(std::string& str, const std::string& delim);

void print_uint(long pos, uint8_t var);