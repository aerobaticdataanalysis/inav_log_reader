/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tools.h"
#include "frames.h"

using namespace std;


FlightLogInfo::FlightLogInfo() {
	motor_0 = -1;
	gps_home[0] = -1;
	gps_home[1] = -1;

}


int get_frame_type(char letter) {
	int frame_type;
	switch (letter) {
	case 'P': frame_type = MAIN_P; break;
	case 'I': frame_type = MAIN_I; break;
	case 'S': frame_type = SPECIAL; break;
	case 'H': frame_type = GPS_H; break;
	case 'G': frame_type = GPS_G; break;
	default: frame_type = UNKNOWN;
	}
	return frame_type;
}


void HistoryValues::update(long new_value) {
	last_values[1] = last_values[0];
	last_values[0] = new_value;
	trust_1 = trust_0;
}

HistoryVec& History::get_frame(int frame_type) {
	switch (frame_type) {
	case SPECIAL: return special;
	case MAIN_I: return main;
	case MAIN_P: return main;
	case GPS_H: return gps_h;
	case GPS_G: return gps;
	default: return unknown;
	}
}

int get_frame_info(std::string value) {
	if (endsWith(value, "name")) return NAME;
	if (endsWith(value, "signed")) return SIGNED;
	if (endsWith(value, "predictor")) return PREDICTOR;
	if (endsWith(value, "encoding")) return ENCODING;
	return UNKNOWN;
}

void FieldConfig::set_value(int frame_info, string value) {
	switch (frame_info) {
	case NAME: name = value; break;
	case SIGNED: sign = stoi(value); break;
	case PREDICTOR: predictor = stoi(value); break;
	case ENCODING: encoding = stoi(value); break;
	}
}


ConfigVec& Config::get_frame(int frame_type) {
	switch (frame_type) {
	case SPECIAL: return special;
	case MAIN_I: return main_i;
	case MAIN_P: return main_p;
	case GPS_H: return gps_h;
	case GPS_G: return gps;
	default: return unknown;
	}
}

std::vector<std::string> Config::frame_names(int frame_type) {
	ConfigVec& frame = get_frame(frame_type);
	vector<string> names;
	for (int i = 0, ilen = frame.size(); i < ilen; i++) {
		names.push_back(frame[i].name);
	}
	return names;
}
