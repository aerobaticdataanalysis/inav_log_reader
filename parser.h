/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <string>
#include <array>
#include <map>
#include "stream.h"
#include "frames.h"


class Header {
public:
	std::map<std::string, std::string> misc_values;
	std::map<std::string, std::string> get_misc_values();
	FlightLogInfo info;
	bool check_row(std::string title, std::string data);
	void parse_header_field_config(std::string title, std::string data);
	void update_value(int frame_type, FieldConfig& config, HistoryValues& history, long value);
	FrameMapping frames;
};


class AveragedField {
private:
	int count = 0;
	float value;
public:
	void new_value(long new_value);
	long get_value();
};


class Parser {
private:
	LogStreamReader reader;
	Header header;
	bool read_header();
	void read_header_row();
	std::vector<long> next_raw_values(ConfigVec& frame_config);
	long apply_predictor(FieldConfig& config, HistoryValues& history, long raw);
public:
	std::vector<Names> field_names;
	bool load_log(std::string filename);
	OutputFrame next_frame();
	OutputDict next_dict();
	std::vector<std::map<std::string, long>> read_all_dicts();
	std::vector<std::vector<double>> read_specific_array(std::vector<std::string> column_names, std::vector<double> factors);
	int max_skips = 100;
	Names get_field_names(int frame_type);
	long current_time = 0;
	bool eof();
	OutputDict next_average_dict(int average_length);
};


