/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <vector>
#include <string>
#include "log.h"
#include "parser.h"
#include "frames.h"


using namespace std;

void LogData::read_all(string filename) {
	Parser logstream;
	if (!logstream.load_log(filename)) 
		return;
	get_columns(logstream.field_names);


}

void LogData::get_columns(vector<Names> field_names) {
	int col_count = 0;
	for (int i = 0; i < field_names.size(); i++) {
		for (int j = 0; j < field_names[i].size(); j++) {
			if (columns.find(field_names[i][j]) == columns.end()) {
				columns[field_names[i][j]] = col_count;
				col_count++;
			}
		}
	}
}