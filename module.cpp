/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/


#include <pybind11/pybind11.h>
#include "pybind11/stl.h"
#include <pybind11/stl_bind.h>
#include "parser.h"
#include "frames.h"
#include "test_runner.h"

namespace py = pybind11;

PYBIND11_MAKE_OPAQUE(std::vector<int>);


PYBIND11_MODULE(inav_log_reader, m) {
	py::class_<OutputFrame>(m, "OutputFrame")
		.def(py::init<>())
		.def_readwrite("frame_type", &OutputFrame::frame_type)
		.def_readwrite("values", &OutputFrame::values);

	py::class_<OutputDict>(m, "OutputDict")
		.def(py::init<>())
		.def_readwrite("time", &OutputDict::time)
		.def_readwrite("pairs", &OutputDict::pairs);

	py::class_<Parser>(m, "Parser")
		.def(py::init<>())
		.def("load_log", &Parser::load_log)
		.def("next_frame", &Parser::next_frame, py::return_value_policy::reference_internal)
		.def("next_dict", &Parser::next_dict, py::return_value_policy::reference_internal)
		.def("next_average_dict", &Parser::next_average_dict, py::return_value_policy::reference_internal)
		.def("read_all_dicts", &Parser::read_all_dicts, py::return_value_policy::reference_internal)
		.def("get_field_names", &Parser::get_field_names)
		.def("read_specific_array", &Parser::read_specific_array, py::return_value_policy::reference_internal)
		.def("eof", &Parser::eof);

	py::class_<Tests>(m, "Tests")
		.def(py::init<>())
		.def("run_test", &Tests::run_test)
		.def("run_all", &Tests::run_all);

#ifdef VERSION_INFO
	m.attr("__version__") = VERSION_INFO;
#else
	m.attr("__version__") = "dev";
#endif
}


