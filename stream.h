/*This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <string>
#include "frames.h"


struct LogStream_t {
	std::string filename;
	const char* data;
	std::size_t size;
	long end, pos;
	int bitPos;
	bool eof;
};


struct LogStreamReader {
	LogStream_t stream;
	void load_log(std::string filename);
	std::string read_line(const char delimiter = '\n');
	long byte_count();
	long current_position();
	uint32_t read_unsigned_vb();
	uint8_t read_byte();
	char read_char();
	char peek_char();
	uint32_t read_bits(int numBits);
	int read_bit();
	void ByteAlign();
	int32_t read_signed_vb();
	void read_tag2_3s32(int64_t* values);
	void read_tag8_4s16_v1(int64_t* values);
	void read_Tag8_4S16_v2(int64_t* values);
	void read_Tag8_8SVB(int64_t* values, int valueCount);
	float read_RawFloat();
	int16_t read_S16();
	uint32_t read_EliasDeltaU32();
	int32_t read_EliasDeltaS32();
	uint32_t read_EliasGammaU32();
	int32_t read_EliasGammaS32();
};


