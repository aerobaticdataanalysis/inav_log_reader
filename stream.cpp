/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <assert.h>
#include <iostream>  
#include <fstream>
#include <string>
#include <vector>

#include "tools.h"
#include "stream.h"
#include "frames.h"

using namespace std;


void LogStreamReader::load_log(string name) {
	stream.filename = name;
	ifstream fl(name, ios::binary | ios::ate);
	fl.seekg(0, ios::end);
	stream.size = fl.tellg();
	stream.end = static_cast<long>(stream.size);
	char* ret = new char[stream.size];
	fl.seekg(0, ios::beg);
	fl.read(ret, stream.size);
	stream.data = ret;
	stream.bitPos = 7;
	fl.close();
	stream.pos = 0;
}

string LogStreamReader::read_line(const char delimiter) {
	int maxlength = 1024;
	string line;

	for (int i = 0; i < maxlength; i++) {
		char c = read_char();

		if (c == delimiter)
			break;

		if (c == EOF || c == '\0')
			break;
		line += c;
	}

	return line;
}

long LogStreamReader::byte_count() {
	return stream.size;
}

long LogStreamReader::current_position() {
	return stream.pos;
}

uint32_t LogStreamReader::read_unsigned_vb() {
	int i, c, shift = 0;
	uint32_t result = 0;
	for (i = 0; i < 5; i++) {
		c = read_byte();
		if (c == EOF) {
			return 0;
		}
		result = result | ((c & ~0x80) << shift);
		if (c < 128) {
			return result;
		}
		shift += 7;
	}
	return 0;
}

uint8_t LogStreamReader::read_byte() {
	if (stream.pos < stream.end) {
		int result = (uint8_t) stream.data[stream.pos];
		// print_uint(stream.pos, result);
		stream.pos++;
		return result;
	}
	stream.eof = true;
	return EOF;
}

char LogStreamReader::read_char() {
	if (stream.pos < stream.end) {
		int result = stream.data[stream.pos];
		// print_uint(stream.pos, result);
		stream.pos++;
		return static_cast<char>(result);
	}
	stream.eof = true;
	return EOF;
}

char LogStreamReader::peek_char()
{
	if (stream.pos < stream.end) {
		int result = stream.data[stream.pos];
		return static_cast<char>(result);
	}

	stream.eof = true;

	return EOF;
}

uint32_t LogStreamReader::read_bits(int numBits) {
	// Round up the bit count to get the byte count
	int numBytes = (numBits + CHAR_BIT - 1) / CHAR_BIT;

	assert(numBits <= 32);

	if (stream.pos + numBytes <= stream.end) {
		uint32_t result = 0;

		while (numBits > 0) {
			result |= ((stream.data[stream.pos] >> stream.bitPos) & 0x01) << (numBits - 1);

			if (stream.bitPos == 0) {
				stream.pos++;
				stream.bitPos = CHAR_BIT - 1;
			}
			else {
				stream.bitPos--;
			}
			numBits--;
		}

		return result;
	}
	else {
		stream.pos = stream.end;
		stream.eof = true;
		stream.bitPos = CHAR_BIT - 1;
		return EOF;
	}
}

int LogStreamReader::read_bit() {
	return read_bits(1);
}

void LogStreamReader::ByteAlign()
{
	if (stream.bitPos != 7) {
		stream.bitPos = 7;
		stream.pos++;
	}
}

int32_t LogStreamReader::read_signed_vb() {
	uint32_t i = read_unsigned_vb();
	return zigzagDecode(i);
}

void LogStreamReader::read_tag2_3s32(int64_t* values) {
	uint8_t leadByte;
	uint8_t byte1, byte2, byte3, byte4;
	int i;
	leadByte = read_byte();

	// Check the selector in the top two bits to determine the field layout
	switch (leadByte >> 6) {
	case 0:
		// 2-bit fields
		values[0] = signExtend2Bit((leadByte >> 4) & 0x03);
		values[1] = signExtend2Bit((leadByte >> 2) & 0x03);
		values[2] = signExtend2Bit(leadByte & 0x03);
		break;
	case 1:
		// 4-bit fields
		values[0] = signExtend4Bit(leadByte & 0x0F);

		leadByte = read_byte();

		values[1] = signExtend4Bit(leadByte >> 4);
		values[2] = signExtend4Bit(leadByte & 0x0F);
		break;
	case 2:
		// 6-bit fields
		values[0] = signExtend6Bit(leadByte & 0x3F);

		leadByte = read_byte();
		values[1] = signExtend6Bit(leadByte & 0x3F);

		leadByte = read_byte();
		values[2] = signExtend6Bit(leadByte & 0x3F);
		break;
	case 3:
		// Fields are 8, 16 or 24 bits, read selector to figure out which field is which size

		for (i = 0; i < 3; i++) {
			switch (leadByte & 0x03) {
			case 0: // 8-bit
				byte1 = read_byte();

				// Sign extend to 32 bits
				values[i] = (int8_t)(byte1);
				break;
			case 1: // 16-bit
				byte1 = read_byte();
				byte2 = read_byte();

				// Sign extend to 32 bits
				values[i] = (int16_t)(byte1 | (byte2 << 8));
				break;
			case 2: // 24-bit
				byte1 = read_byte();
				byte2 = read_byte();
				byte3 = read_byte();

				values[i] = signExtend24Bit(byte1 | (byte2 << 8) | (byte3 << 16));
				break;
			case 3: // 32-bit
				byte1 = read_byte();
				byte2 = read_byte();
				byte3 = read_byte();
				byte4 = read_byte();

				// Sign-extend
				values[i] = (int32_t)(byte1 | (byte2 << 8) | (byte3 << 16) | (byte4 << 24));
				break;
			}

			leadByte >>= 2;
		}
		break;
	}
}

void LogStreamReader::read_tag8_4s16_v1(int64_t* values) {
	uint8_t selector, combinedChar;
	uint8_t char1, char2;
	int i;

	enum {
		FIELD_ZERO = 0,
		FIELD_4BIT = 1,
		FIELD_8BIT = 2,
		FIELD_16BIT = 3
	};

	selector = read_byte();

	//Read the 4 values from the stream
	for (i = 0; i < 4; i++) {
		switch (selector & 0x03) {
		case FIELD_ZERO:
			values[i] = 0;
			break;
		case FIELD_4BIT: // Two 4-bit fields
			combinedChar = (uint8_t)read_byte();

			values[i] = signExtend4Bit(combinedChar & 0x0F);

			i++;
			selector >>= 2;

			values[i] = signExtend4Bit(combinedChar >> 4);
			break;
		case FIELD_8BIT: // 8-bit field
			//Sign extend...
			values[i] = (int8_t)read_byte();
			break;
		case FIELD_16BIT: // 16-bit field
			char1 = read_byte();
			char2 = read_byte();

			//Sign extend...
			values[i] = (int16_t)(char1 | (char2 << 8));
			break;
		}

		selector >>= 2;
	}
}

void LogStreamReader::read_Tag8_4S16_v2(int64_t* values) {
	uint8_t selector;
	uint8_t char1, char2;
	uint8_t buffer;
	int nibbleIndex;

	int i;

	enum {
		FIELD_ZERO = 0,
		FIELD_4BIT = 1,
		FIELD_8BIT = 2,
		FIELD_16BIT = 3
	};

	selector = read_byte();

	//Read the 4 values from the stream
	nibbleIndex = 0;
	for (i = 0; i < 4; i++) {
		switch (selector & 0x03) {
		case FIELD_ZERO:
			values[i] = 0;
			break;
		case FIELD_4BIT:
			if (nibbleIndex == 0) {
				buffer = (uint8_t)read_byte();
				values[i] = signExtend4Bit(buffer >> 4);
				nibbleIndex = 1;
			}
			else {
				values[i] = signExtend4Bit(buffer & 0x0F);
				nibbleIndex = 0;
			}
			break;
		case FIELD_8BIT:
			if (nibbleIndex == 0) {
				//Sign extend...
				values[i] = (int8_t)read_byte();
			}
			else {
				char1 = buffer << 4;
				buffer = (uint8_t)read_byte();

				char1 |= buffer >> 4;
				values[i] = (int8_t)char1;
			}
			break;
		case FIELD_16BIT:
			if (nibbleIndex == 0) {
				char1 = (uint8_t)read_byte();
				char2 = (uint8_t)read_byte();

				//Sign extend...
				values[i] = (int16_t)(uint16_t)((char1 << 8) | char2);
			}
			else {
				/*
				 * We're in the low 4 bits of the current buffer, then one byte, then the high 4 bits of the next
				 * buffer.
				 */
				char1 = (uint8_t)read_byte();
				char2 = (uint8_t)read_byte();

				values[i] = (int16_t)(uint16_t)((buffer << 12) | (char1 << 4) | (char2 >> 4));

				buffer = char2;
			}
			break;
		}

		selector >>= 2;
	}
}

void LogStreamReader::read_Tag8_8SVB(int64_t* values, int valueCount)
{
	uint8_t header;

	if (valueCount == 1) {
		values[0] = read_unsigned_vb();
	}
	else {
		header = (uint8_t)read_byte();

		for (int i = 0; i < 8; i++, header >>= 1)
			values[i] = (header & 0x01) ? read_signed_vb() : 0;
	}
}

float LogStreamReader::read_RawFloat()
{
	union floatConvert_t {
		float f;
		uint8_t bytes[4];
	} floatConvert;

	for (int i = 0; i < 4; i++) {
		floatConvert.bytes[i] = read_byte();
	}

	return floatConvert.f;
}

int16_t LogStreamReader::read_S16() {
	return read_byte() | (read_byte() << 8);
}

uint32_t LogStreamReader::read_EliasDeltaU32()
{
	const int MAX_BIT_READ_SIZE = 32;

	int lengthValBits = 0;
	uint8_t length;
	uint32_t lengthLowBits, resultLowBits;
	uint32_t result;

	while (lengthValBits <= MAX_BIT_READ_SIZE && read_bit() == 0) {
		lengthValBits++;
	}

	if (stream.eof || lengthValBits > MAX_BIT_READ_SIZE) {
		return 0;
	}

	// Now we know the length of the field used to store the length of the encoded value, so read those length bits
	lengthLowBits = read_bits(lengthValBits);

	if (stream.eof) {
		return 0;
	}

	length = ((1 << lengthValBits) | lengthLowBits) - 1;

	if (length > MAX_BIT_READ_SIZE) {
		//Corrupt value
		return 0;
	}

	// Now we know the length of the encoded value, so read those bits
	resultLowBits = read_bits(length);

	if (stream.eof) {
		return 0;
	}

	result = (1 << length) | resultLowBits;

	// The highest value is an escape code that means either MAXINT - 1 or MAXINT depending on the following bit
	if (result == 0xFFFFFFFF) {
		int escapeVal = read_bit();

		if (escapeVal == 0) {
			return 0xFFFFFFFF - 1;
		}
		else if (escapeVal == 1) {
			return 0xFFFFFFFF;
		}
		else {
			//EOF
			return 0;
		}
	}

	return result - 1;
}

int32_t LogStreamReader::read_EliasDeltaS32() {
	return zigzagDecode(LogStreamReader::read_EliasDeltaU32());
}

uint32_t LogStreamReader::read_EliasGammaU32() {

	const int MAX_BIT_READ_SIZE = 32;

	int valBits = 0;
	uint32_t valueLowBits;
	uint32_t result;

	while (valBits <= MAX_BIT_READ_SIZE && read_bit() == 0) {
		valBits++;
	}

	if (stream.eof || valBits > MAX_BIT_READ_SIZE) {
		return 0;
	}

	// We've read the first 1 bit of the encoded value, now read the rest of the bits
	valueLowBits = read_bits(valBits - 1);

	if (stream.eof) {
		return 0;
	}

	result = (1 << (valBits - 1)) | valueLowBits;

	// The highest value is an escape code that means either MAXINT - 1 or MAXINT depending on the following bit
	if (result == 0xFFFFFFFF) {
		int escapeVal = read_bit();

		if (escapeVal == 0) {
			return 0xFFFFFFFF - 1;
		}
		else if (escapeVal == 1) {
			return 0xFFFFFFFF;
		}
		else {
			//EOF
			return 0;
		}
	}

	return result - 1;
}

int32_t LogStreamReader::read_EliasGammaS32() {
	return zigzagDecode(read_EliasGammaU32());
}

