/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string.h>
#include <vector>
#include "tools.h"

using namespace std;


int32_t zigzagDecode(uint32_t value) {
	return (value >> 1) ^ -(int32_t)(value & 1);
}

int32_t signExtend24Bit(uint32_t u) {
	return (u & 0x800000) ? (int32_t)(u | 0xFF000000) : (int32_t)u;
}

int32_t signExtend14Bit(uint16_t word) {
	return (word & 0x2000) ? (int32_t)(int16_t)(word | 0xC000) : word;
}

int32_t signExtend6Bit(uint8_t byte) {
	return (byte & 0x20) ? (int32_t)(int8_t)(byte | 0xC0) : byte;
}

int32_t signExtend4Bit(uint8_t nibble) {
	return (nibble & 0x08) ? (int32_t)(int8_t)(nibble | 0xF0) : nibble;
}

int32_t signExtend2Bit(uint8_t byte) {
	return (byte & 0x02) ? (int32_t)(int8_t)(byte | 0xFC) : byte;
}


bool startsWith(string s, string beginning) {
	if (s.length() >= beginning.length()) {
		return (0 == s.compare(0, beginning.length(), beginning));
	}
	else {
		return false;
	}
}

bool endsWith(string s, string ending) {
	if (s.length() >= ending.length()) {
		return (0 == s.compare(s.length() - ending.length(), ending.length(), ending));
	}
	else {
		return false;
	}
}

vector<string> split(const string& str, const string& delim) {
	vector<string> tokens;
	size_t prev = 0, pos = 0;
	do
	{
		pos = str.find(delim, prev);
		if (pos == string::npos)
			pos = str.length();
		string token = str.substr(prev, pos - prev);
		if (!token.empty()) 
			tokens.push_back(token);
		prev = pos + delim.length();
	} while (pos < str.length() && prev < str.length());
	return tokens;
}

vector<int> split_int(string& str, const string& delim) {
	std::vector<string> tokens = split(str, delim);
	std::vector<int> values;
	for (int i = 0, ilen = tokens.size(); i < ilen; i++) {
		int value = stoi(tokens[i]);
		values.push_back(value);
	}
	return values;
}

void print_uint(long pos, uint8_t var) {
	cout << "position: " << pos << " value: " << (int)var << " char: " << var << std::endl;
}