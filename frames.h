/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <map>
#include <string>
#include <vector>


struct FlightLogInfo {
	FlightLogInfo();
	int data_version;
	int i_interval;
	int p_interval[2];
	int p_inv_ratio;
	long current_iteration = 0;
	long minthrottle;
	long maxthrottle;
	long motor_0; // TODO set initial values to -1
	std::string firmware_revision;
	long servos = 1500;
	long vbatref;
	long gps_home[2];  // TODO set initial values to -1
	long last_main_frame_time = 0;
	long motor_output_low;
	bool values_initiated;
};


enum FlightLogFieldPredictor {
	FLIGHT_LOG_FIELD_PREDICTOR_0 = 0,
	FLIGHT_LOG_FIELD_PREDICTOR_PREVIOUS = 1,
	FLIGHT_LOG_FIELD_PREDICTOR_STRAIGHT_LINE = 2,
	FLIGHT_LOG_FIELD_PREDICTOR_AVERAGE_2 = 3,
	FLIGHT_LOG_FIELD_PREDICTOR_MINTHROTTLE = 4,
	FLIGHT_LOG_FIELD_PREDICTOR_MOTOR_0 = 5,
	FLIGHT_LOG_FIELD_PREDICTOR_INC = 6,
	FLIGHT_LOG_FIELD_PREDICTOR_HOME_COORD = 7,
	FLIGHT_LOG_FIELD_PREDICTOR_1500 = 8,
	FLIGHT_LOG_FIELD_PREDICTOR_VBATREF = 9,
	FLIGHT_LOG_FIELD_PREDICTOR_LAST_MAIN_FRAME_TIME = 10,
	FLIGHT_LOG_FIELD_PREDICTOR_MINMOTOR = 11,
	FLIGHT_LOG_FIELD_PREDICTOR_HOME_COORD_1 = 12,
};


enum FlightLogFieldEncoding {
	FLIGHT_LOG_FIELD_ENCODING_SIGNED_VB = 0,
	FLIGHT_LOG_FIELD_ENCODING_UNSIGNED_VB = 1,
	FLIGHT_LOG_FIELD_ENCODING_NEG_14BIT = 3,
	FLIGHT_LOG_FIELD_ENCODING_ELIAS_DELTA_U32 = 4,
	FLIGHT_LOG_FIELD_ENCODING_ELIAS_DELTA_S32 = 5,
	FLIGHT_LOG_FIELD_ENCODING_TAG8_8SVB = 6,
	FLIGHT_LOG_FIELD_ENCODING_TAG2_3S32 = 7,
	FLIGHT_LOG_FIELD_ENCODING_TAG8_4S16 = 8,
	FLIGHT_LOG_FIELD_ENCODING_NULL = 9,
	FLIGHT_LOG_FIELD_ENCODING_ELIAS_GAMMA_U32 = 10,
	FLIGHT_LOG_FIELD_ENCODING_ELIAS_GAMMA_S32 = 11
};


enum FlightLogFieldSign {
	FLIGHT_LOG_FIELD_UNSIGNED = 0,
	FLIGHT_LOG_FIELD_SIGNED = 1
};


enum FrameTypes {
	SPECIAL = 0,
	MAIN_I = 1,
	MAIN_P = 2,
	GPS_H = 3,
	GPS_G = 4,
	UNKNOWN = 5
};


int get_frame_type(char letter);


struct HistoryValues {
	long last_values[2];
	void update(long new_value);
	bool trust_0 = false;
	bool trust_1 = false;
};

typedef std::vector<HistoryValues> HistoryVec;

struct History {
	HistoryVec main;
	HistoryVec gps;
	HistoryVec gps_h;
	HistoryVec special;
	HistoryVec unknown;

	HistoryVec& get_frame(int frame_type);

};

enum FrameInfo {
	NAME = 0,
	SIGNED = 1,
	PREDICTOR = 2,
	ENCODING = 3,
};  // TODO this name doesnt really describe the contents

int get_frame_info(std::string name);

struct FieldConfig {
	std::string name;
	int sign;
	int encoding;
	int predictor;
	void set_value(int frame_info, std::string value);

};

typedef std::vector<FieldConfig> ConfigVec;
typedef std::vector<std::string> Names;

struct Config {
	ConfigVec main_i;
	ConfigVec main_p;
	ConfigVec gps;
	ConfigVec gps_h;
	ConfigVec special;
	ConfigVec unknown;

	ConfigVec& get_frame(int frame_type);
	Names frame_names(int frame_type);
};


struct FrameMapping {
	Config config;
	History history;
};

struct OutputFrame {
	int frame_type;
	std::vector<long> values;
};

struct OutputDict {
	long time;
	std::map<std::string, long> pairs;
};