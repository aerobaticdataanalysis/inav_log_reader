/*
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <array>
#include <iostream>
#include "parser.h"
#include "tools.h"
#include "frames.h"
#include <limits>
#include <cmath>

using namespace std;


bool Header::check_row(string title, string data) {

	if (title == "Data version") { info.data_version = stoi(data); return true; }
	if (title == "I interval") { info.i_interval = stoi(data); return true; }
	if (title == "P interval") {
		vector<int> values = split_int(data, "/");  
		info.p_interval[0] = values[0];
		info.p_interval[1] = values[1];
		return true;
	}
	if (title == "minthrottle") { info.minthrottle = stoi(data); return true; }
	if (title == "maxthrottle") { info.maxthrottle = stoi(data); return true; }
	if (title == "Firmware revision") { info.firmware_revision = data; return true; }
	if (title == "vbatref") { info.vbatref = stoi(data); return true; }
	return false;
}


bool Parser::read_header() {
	while (reader.peek_char() == 'H') {
		reader.stream.pos += 2;
		read_header_row();
	}
	return true;
}


void Parser::read_header_row() {
	string title = reader.read_line(':');
	string data = reader.read_line();
	
	if (startsWith(title, "Field")) {
		header.parse_header_field_config(title, data);
	} else if (!header.check_row(title, data)) {
		header.misc_values[title] = data;
	}
		
}


void Header::parse_header_field_config(string title, string data) {
	int frame_info = get_frame_info(title);
	int frame_type = get_frame_type(title[6]);
	
	ConfigVec& frame_config = frames.config.get_frame(frame_type);
	HistoryVec& frame_history = frames.history.get_frame(frame_type);

	// TODO the following should be moved into a function in the FrameMapping class
	vector<string> frame_data = split(data, ",");
	int ilim = frame_data.size();
	if (frame_config.size() < ilim) {
		frame_config.resize(ilim);  // TODO raise exception
		frame_history.resize(ilim);
	}
	for (int i = 0; i < ilim; i++) {
		frame_config[i].set_value(frame_info, frame_data[i]);
	}

	if (frame_type == MAIN_I) {
		if (frame_info == NAME || frame_info == SIGNED)

			if (frames.config.main_p.size() < ilim)
				frames.config.main_p.resize(ilim);

			for (int i = 0; i < ilim; i++) {
				frames.config.main_p[i].set_value(frame_info, frame_data[i]);
			}
	}
}


void Header::update_value(int frame_type, FieldConfig& config, HistoryValues& history, long value) {
	history.last_values[1] = history.last_values[0];
	history.last_values[0] = value;
	history.trust_1 = (history.trust_0 && !(frame_type == MAIN_I));
	history.trust_0 = true;
	if (!info.values_initiated) {
		if (config.name == "GPS_home[0]") info.gps_home[0] = value;
		if (config.name == "GPS_home[1]") info.gps_home[1] = value;
		if (info.gps_home[0] >= 0 && info.gps_home[1] >= 0) info.values_initiated = true;
	}
}


map<string, string> Header::get_misc_values() {
	return misc_values;
}



bool Parser::load_log(string filename) {
	reader.load_log(filename);
	if (!read_header())
		return false;
	for (int i = 0; i < 5; i++) {
		field_names.push_back(header.frames.config.frame_names(i));
	}
	return true;
}


Names Parser::get_field_names(int frame_type) {
	return field_names[frame_type];
}


OutputFrame Parser::next_frame() {
	OutputFrame next_frame;
	int i = 0;
	next_frame.frame_type = get_frame_type(reader.read_char());
	while (next_frame.frame_type == UNKNOWN) {
		next_frame.frame_type = get_frame_type(reader.read_char());
		i++;
		if (i >= max_skips) return next_frame;
	}
		
	ConfigVec& frame_config = header.frames.config.get_frame(next_frame.frame_type);
	HistoryVec& frame_history = header.frames.history.get_frame(next_frame.frame_type);

	vector<long> raw_values = next_raw_values(frame_config);
	
	
	for (int i = 0, ilen = frame_config.size(); i < ilen; i++) {
		next_frame.values.push_back(apply_predictor(frame_config[i], frame_history[i], raw_values[i]));
		header.update_value(next_frame.frame_type, frame_config[i], frame_history[i], next_frame.values[i]);
	}

	if (next_frame.frame_type == MAIN_I | next_frame.frame_type == MAIN_P) current_time = next_frame.values[1];
	

	return next_frame;
}

OutputDict Parser::next_dict() {
	OutputFrame frame = next_frame();
	OutputDict dict;
	for (int i = 0, ilen = frame.values.size(); i < ilen; i++) {
		dict.time = current_time;
		dict.pairs[field_names[frame.frame_type][i]] = frame.values[i];
	}
	return dict;
}

vector<long> Parser::next_raw_values(ConfigVec& frame_config) {
	int field_count = frame_config.size();
	vector<long> values;
	long vals[8];
	int i = 0; int j = 0; int groupCount;
	while (i < field_count) {
		int64_t vals[8];

		switch (frame_config[i].encoding) {
		case FLIGHT_LOG_FIELD_ENCODING_SIGNED_VB:
			reader.ByteAlign();
			values.push_back(reader.read_signed_vb());
			break;
		case FLIGHT_LOG_FIELD_ENCODING_UNSIGNED_VB:
			reader.ByteAlign();
			values.push_back(reader.read_unsigned_vb());
			break;
		case FLIGHT_LOG_FIELD_ENCODING_NEG_14BIT:
			reader.ByteAlign();

			values.push_back(-signExtend14Bit(reader.read_unsigned_vb()));
			break;
		case FLIGHT_LOG_FIELD_ENCODING_TAG8_4S16:
			reader.ByteAlign();

			if (header.info.data_version < 2)
				reader.read_tag8_4s16_v1(vals);
			else
				reader.read_Tag8_4S16_v2(vals);
			for (j = 0; j < 4; j++, i++)
				values.push_back(vals[j]);
			continue;
			break;
		case FLIGHT_LOG_FIELD_ENCODING_TAG2_3S32:
			reader.ByteAlign();
			reader.read_tag2_3s32(vals);
			for (j = 0; j < 3; j++, i++)
				values.push_back(vals[j]);
			continue;
			break;
		case FLIGHT_LOG_FIELD_ENCODING_TAG8_8SVB:
			reader.ByteAlign();
			for (j = i + 1; j < i + 8 && j < field_count; j++)
				if (frame_config[j].encoding != FLIGHT_LOG_FIELD_ENCODING_TAG8_8SVB)
					break;
			groupCount = j - i;
			reader.read_Tag8_8SVB(vals, groupCount);
				for (j = 0; j < groupCount; j++, i++)
					values.push_back(vals[j]);
			continue;
			break;
		case FLIGHT_LOG_FIELD_ENCODING_ELIAS_DELTA_U32:
			values.push_back(reader.read_EliasDeltaU32());
			break;
		case FLIGHT_LOG_FIELD_ENCODING_ELIAS_DELTA_S32:
			values.push_back(reader.read_EliasDeltaS32());
			break;
		case FLIGHT_LOG_FIELD_ENCODING_ELIAS_GAMMA_U32:
			values.push_back(reader.read_EliasGammaU32());
			break;
		case FLIGHT_LOG_FIELD_ENCODING_ELIAS_GAMMA_S32:
			values.push_back(reader.read_EliasGammaS32());
			break;
		case FLIGHT_LOG_FIELD_ENCODING_NULL:
			//Nothing to read
			values.push_back(0);
			break;
		default:
			exit(-1);  // TODO raise an exception here
		}

		//value = applyPrediction(log, i, raw ? FLIGHT_LOG_FIELD_PREDICTOR_0 : predictor[i], value, frame, previous, previous2);
		i++;
		
	}
	reader.ByteAlign();
	return values;
}


long Parser::apply_predictor(FieldConfig& config, HistoryValues& history, long value) {
	switch (config.predictor) {
	case FLIGHT_LOG_FIELD_PREDICTOR_INC: value += history.last_values[0] + (int) (0.5 +  header.info.p_interval[1] / header.info.p_interval[0]);  // TODO get number of skipped frames from somewhere.
	case FLIGHT_LOG_FIELD_PREDICTOR_0: break;
	case FLIGHT_LOG_FIELD_PREDICTOR_MINTHROTTLE: value += header.info.minthrottle; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_1500: value += 1500; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_MOTOR_0: if (!header.info.motor_0 < 0) value += header.info.motor_0; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_VBATREF: value += header.info.vbatref; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_PREVIOUS: if (history.trust_0) value += history.last_values[0]; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_STRAIGHT_LINE: 
		if (history.trust_1 && history.trust_0)
			value += 2 * history.last_values[0] - history.last_values[1];
		else if (history.trust_0)
			value += history.last_values[0];
		break;
	case FLIGHT_LOG_FIELD_PREDICTOR_AVERAGE_2: 
		if (history.trust_0 && history.trust_1)
			value += (history.last_values[0] + history.last_values[1]) / 2;
		else if (history.trust_0) 
			value += history.last_values[0];
		break;
	case FLIGHT_LOG_FIELD_PREDICTOR_HOME_COORD: if (!header.info.gps_home[0] < 0) value += header.info.gps_home[0]; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_HOME_COORD_1: if (!header.info.gps_home[1] < 0) value += header.info.gps_home[1]; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_LAST_MAIN_FRAME_TIME: value += header.info.last_main_frame_time; break;
	case FLIGHT_LOG_FIELD_PREDICTOR_MINMOTOR: value += header.info.motor_output_low; break;
	default:
		break;
	}

	return value;
}

bool Parser::eof() {
	return reader.stream.eof;
}


OutputDict Parser::next_average_dict(int average_length) {
	long end_time = current_time + average_length;
	vector<AveragedField> values;
	vector<string> names;
	map<int, int> frame_start_positions;
	while (current_time < end_time && !reader.stream.eof) {
		OutputFrame new_frame = next_frame();
		int ilen = new_frame.values.size();
		if (frame_start_positions.find(new_frame.frame_type) == frame_start_positions.end()) {
			frame_start_positions[new_frame.frame_type] = values.size();
			for (int i = 0; i < ilen; i++) {
				AveragedField field;
				values.push_back(field);
				names.push_back(field_names[new_frame.frame_type][i]);
			}
		}
		for (int i = 0; i < ilen; i++) {
			values[frame_start_positions[new_frame.frame_type] + i].new_value(new_frame.values[i]);
		}
	}

	OutputDict result;
	result.time = end_time - average_length / 2;
	for (int i = 0, ilen = values.size(); i < ilen; i++) {
		result.pairs[names[i]] = values[i].get_value();
	}
	return result;
}


std::vector<std::map<std::string, long>> Parser::read_all_dicts() {
	std::vector<std::map<std::string, long>> all_dicts;
	while (!eof()) {

		OutputDict dict = next_dict();
		dict.pairs["time"] = dict.time;
		all_dicts.push_back(dict.pairs);
	}
	return all_dicts;
}

vector<vector<double>> Parser::read_specific_array(vector<string> column_names, vector<double> factors) {
	// returns array of data for specified column names
	int n_cols = column_names.size();
	vector<vector<double>> interim_result(0, std::vector<double>(n_cols, numeric_limits<double>::quiet_NaN()));
	vector<map<int, int>> frame_output_maps;

	// TODO 5 should not be hard coded and the following loop should be in its own function
	for (int i = 0; i < 5; i++) {
		map<int, int> output_map;
		Names frame_names = header.frames.config.frame_names(i);
		for (int j = 0; j < frame_names.size(); j++) {
			for (int k = 0; k < n_cols; k++) {
				if (column_names[k] == frame_names[j]) {
					output_map[j] = k;
				}
			}
		}
		frame_output_maps.push_back(output_map);
	}
	//frame_output_maps = [{field position id, output column id}]

	map<int, int>::iterator it;
	long last_time = -1;
	while (!eof()) {
		
		OutputFrame frame = next_frame();
		
		if (!(frame.frame_type == UNKNOWN)) {
			if (last_time == current_time) {
				for (it = frame_output_maps[frame.frame_type].begin(); it != frame_output_maps[frame.frame_type].end(); it++) {
					if (isnan(interim_result.back()[it->second])) 
						interim_result.back()[it->second] = frame.values[it->first] * factors[it->second];		
				}
			}
			else {
				std::vector<double> next_row(n_cols, numeric_limits<double>::quiet_NaN());
				for (it = frame_output_maps[frame.frame_type].begin(); it != frame_output_maps[frame.frame_type].end(); it++) {
					next_row[it->second] = frame.values[it->first] * factors[it->second];
				}

				interim_result.push_back(next_row);
			}
		}
		last_time = current_time;
	}

	return interim_result;
}


void AveragedField::new_value(long new_value) {
	value = (count * value + new_value) / (count + 1);
	count++;
}

long AveragedField::get_value() {
	return 0.5 + value;
}